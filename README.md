The library implements helpers, services and other classes for code generation of command classes, models, namespace by ddd practices, etc.

Install this library in your project:
```bash
composer config repositories.b-public composer https://git.birb.pro/api/v4/group/b-public/-/packages/composer/packages.json
composer require birb/fancy-stubs-codegen
```

Navigation for doc:

1. [Why need this library](/doc/whyFancyStubsCodegen.md)
2. [Use Postprocessing](/doc/usePostProcessing.md)
