<?php

namespace Birb\FancyStubsCodegen;

use Birb\FancyStubsCodegen\CreateClassService\Attributes\Visibility\RemoveFromRender;
use Birb\FancyStubsCodegen\ValueObjects\AttributeTokenVo;
use Birb\FancyStubsCodegen\Wrappers\ClassTypeContext;

readonly abstract class WithContextClassAbstract implements RemoveFromRender
{
    public function __construct(
        public ClassTypeContext $classTypeContext,
        public AttributeTokenVo $attributeTokenVo
    )
    {
    }
    public abstract function handle(): void;
}
