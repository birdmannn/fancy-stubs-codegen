<?php

namespace Birb\FancyStubsCodegen\Wrappers;

use Birb\FancyStubsCodegen\ValueObjects\PrinterVO;
use Nette\PhpGenerator\Printer;
use ReflectionClass;

class PrinterWrapper extends Printer
{
    public function __construct(
        PrinterVO $printerVO
    )
    {
        $params = (new ReflectionClass($printerVO))->getProperties();
        foreach ($params as $param) {
            if(!(isset($this->{$param->getName()}))) {
                continue;
            }
            $this->{$param->getName()} = $param->getValue($printerVO);
        }
        parent::__construct();
    }
}
