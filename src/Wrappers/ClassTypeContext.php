<?php

namespace Birb\FancyStubsCodegen\Wrappers;

use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

readonly class ClassTypeContext
{
    public function __construct(
        public ClassType $classType,
        public PhpNamespace $namespace
    )
    {
    }

}
