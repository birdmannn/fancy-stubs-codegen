<?php

namespace Birb\FancyStubsCodegen\CreateClassService\Definers;

use Birb\FancyStubsCodegen\WithContextClassAbstract\PutDefaultValue;
use Illuminate\Support\Str;

readonly class LaravelConventForIdsUseClassNameContextPutDefaultValue extends PutDefaultValue
{
    public function getValue(): string
    {
        return sprintf('%s_id', Str::snake(
            Str::singular(
                $this->classTypeContext->classType->getName()
            )
        )); // on input Flight on return flight_id
    }
}
