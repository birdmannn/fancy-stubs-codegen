<?php

namespace Birb\FancyStubsCodegen\CreateClassService\Definers;

use Birb\FancyStubsCodegen\WithContextClassAbstract\PutDefaultValue;
use Illuminate\Support\Str;

readonly class LaravelConventForTableUseClassNameContextPutDefaultValue extends PutDefaultValue
{
    public function getValue(): string
    {
        return Str::snake(
            Str::plural(
                $this->classTypeContext->classType->getName()
            )
        ); // on input Flight on return flights
    }
}
