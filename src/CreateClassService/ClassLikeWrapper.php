<?php

namespace Birb\FancyStubsCodegen\CreateClassService;

use Birb\FancyStubsCodegen\ValueObjects\ClassVO;
use Birb\FancyStubsCodegen\Wrappers\ClassTypeContext;
use Nette\PhpGenerator\Attribute;
use Nette\PhpGenerator\ClassLike;
use Nette\PhpGenerator\ClassType;
use Nette\PhpGenerator\PhpNamespace;

readonly class ClassLikeWrapper
{
    public ClassType $classType;

    public function __construct(
        public ClassVO $classVO,
        public PhpNamespace $namespace,
        private ?ClassTypeContext $classTypeContext = null
    )
    {
        $this->classType = $this->getClassType();
        $this->namespace->add($this->classType);
    }

    /**
     * @return ClassTypeContext
     */
    public function getClassTypeContext(): ClassTypeContext
    {
        return $this->classTypeContext ?? new ClassTypeContext(
            $this->classType,
            $this->namespace
        );
    }

    /**
     * @return ClassType
     */
    private function getClassType(): ClassType
    {
        return ClassLike::fromCode($this->classVO->code); // @phpstan-ignore-line
    }

    /**
     * @return array<Attribute>
     */
    public function getAttributes(): array
    {
        return array_merge(
            $this->classType->getAttributes(),
            $this->classVO->additionalAttributes
        );
    }

    /**
     * @param  Attribute[]  $attrs
     */
    public function setAttributes(array $attrs): ClassType
    {
        return $this->classType->setAttributes($attrs);
    }
}
