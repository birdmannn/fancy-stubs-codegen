<?php

namespace Birb\FancyStubsCodegen\CreateClassService\Attributes\Exception;

use Exception;

class NotDefinedException extends Exception
{
}
