<?php

namespace Birb\FancyStubsCodegen\CreateClassService\Attributes;

use Attribute;
use Birb\FancyStubsCodegen\CreateClassService\Attributes\Exception\NotDefinedException;
use Birb\FancyStubsCodegen\CreateClassService\Attributes\Visibility\RemoveFromRender;
use Birb\FancyStubsCodegen\ValueObjects\AttributeTokenVo;
use Birb\FancyStubsCodegen\WithContextClassAbstract;
use Birb\FancyStubsCodegen\Wrappers\ClassTypeContext;
use Illuminate\Support\Str;
use Nette\PhpGenerator\Attribute as NetteAttribute;
use Nette\PhpGenerator\Dumper;
use Nette\PhpGenerator\Literal;

#[Attribute]
class DefinerAttribute implements RemoveFromRender
{
    /**
     * @param Literal|class-string<WithContextClassAbstract> $definerClass
     */
    public function __construct(
        readonly public string $definerClass
    )
    {
    }

    /**
     * @throws NotDefinedException
     */
    public static function newInstanceOfDefiner(
        NetteAttribute $attribute,
        AttributeTokenVo $token,
        ClassTypeContext $classTypeContext
    ): WithContextClassAbstract
    {
        /** @var object|DefinerAttribute $args */
        $args = (object)$attribute->getArguments();
        if (!isset($args->definerClass)) {
            throw new NotDefinedException();
        }
        return new (self::parseFromLiteralClass($args->definerClass)) (
            $classTypeContext,
            $token
        );
    }

    /**
     * TODO Странный костыль, надо поправить
     *
     * @param Literal $definerClass
     * @return class-string<WithContextClassAbstract>
     *
     */
    private static function parseFromLiteralClass(Literal $definerClass): string
    {
        $rawFormatting = $definerClass->formatWith(new Dumper());
        return Str::replace(['/*(n*/', '::class'], '', $rawFormatting);
    }

    /**
     * @param string $class
     * @return NetteAttribute
     */
    public static function makeDefiner(string $class): NetteAttribute
    {
        return (new NetteAttribute(self::class, [
            'definerClass' => new Literal(
                $class,
                null
            )
        ]));
    }

    /**
     * @return string
     */
    public function getDefinerClassInst(): WithContextClassAbstract
    {
        return $this->definerClass;
    }

}
