<?php

namespace Birb\FancyStubsCodegen\Mixins;

use ReflectionClass;
use ReflectionException;

class SoftReflectionHelper
{

    /**
     * @param string $attributeClass
     * @return mixed
     * @throws ReflectionException
     */
    public static function createInstanceByClass(
        string $attributeClass
    ): mixed
    {
        return ((new ReflectionClass($attributeClass))->newInstanceWithoutConstructor());
    }

}
