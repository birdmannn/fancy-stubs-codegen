<?php

namespace Birb\FancyStubsCodegen\Postprocessing;

class PhpCbfOnStringCode
{
    public function __construct(
        public string $standard
    )
    {
    }

    public function build(
        string $classCode
    ): string
    {
        // Write tmp file with code
        $tmpFilename = sprintf('%s/%s.php', sys_get_temp_dir(), sha1(date('d-m-Y-H-i-s')));
        file_put_contents($tmpFilename, $classCode);
        chmod($tmpFilename, 666);

        shell_exec(
            sprintf(
                'php -d memory_limit=1024M vendor/bin/phpcbf --standard=%s %s',
                $this->standard,
                $tmpFilename
            )
        );

        $result = file_get_contents($tmpFilename);

        unlink($tmpFilename);
        return $result;
    }
}
