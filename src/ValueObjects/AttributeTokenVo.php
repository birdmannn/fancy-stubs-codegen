<?php

namespace Birb\FancyStubsCodegen\ValueObjects;

use Birb\FancyStubsCodegen\CreateClassService\Attributes\Visibility\RemoveFromRender;
use Birb\FancyStubsCodegen\CreateClassService\ClassLikeWrapper;
use Birb\FancyStubsCodegen\Mixins\SoftReflectionHelper;
use Nette\PhpGenerator\Attribute;
use Nette\PhpGenerator\Constant;
use Nette\PhpGenerator\Method;
use Nette\PhpGenerator\Property;
use ReflectionException;

readonly class AttributeTokenVo
{
    /**
     * @param Constant|Property|ClassLikeWrapper $protoToken
     */
    public function __construct(
        private Constant|Property|ClassLikeWrapper $protoToken
    )
    {
    }

    /**
     * @return Attribute[]
     * @throws ReflectionException
     */
    public function getAvailableAttributes(): array
    {
        return array_filter($this->protoToken->getAttributes(), function (Attribute $attribute) {
            return SoftReflectionHelper::createInstanceByClass($attribute->getName()) instanceof RemoveFromRender;
        });
    }

    /**
     * @return Attribute[]
     * @throws ReflectionException
     */
    public function getAttributes(): array
    {
        return $this->protoToken->getAttributes();
    }

    public function setValue($value): void
    {
        if($value instanceof Method) {
            $this->protoToken->setComment($value);
            return;
        }
        $this->protoToken->setValue($value);
    }

    /**
     * @param Attribute[] $attributes
     * @return void
     * @throws ReflectionException
     */
    public function setAttributes(
        array $attributes
    ): void
    {
        $allAttributesArr = array_map(function (Attribute $attribute) {
            return $attribute->getName();
        }, $this->getAvailableAttributes());
        $this->protoToken->setAttributes(array_filter($attributes, function (Attribute $attribute) use ($allAttributesArr) {
            return !in_array($attribute->getName(), $allAttributesArr);
        }));
    }

    public function getName(): string
    {
        return $this->protoToken->getName();
    }

}
