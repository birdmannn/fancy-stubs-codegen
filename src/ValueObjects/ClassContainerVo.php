<?php

namespace Birb\FancyStubsCodegen\ValueObjects;

use ReflectionClass;

readonly class ClassContainerVo
{

    public function __construct(
        public ReflectionClass $reflectionClass
    )
    {
    }

}
