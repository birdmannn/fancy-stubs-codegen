<?php

namespace Birb\FancyStubsCodegen\ValueObjects;

use Nette\PhpGenerator\Dumper;
use Nette\PhpGenerator\PhpNamespace;

readonly class PrinterVO
{

    public Dumper $dumper;

    public function __construct(
        public bool             $resolveTypes,
        ?Dumper                 $dumper = null,
        public int              $wrapLength = 120,
        public string           $indentation = "\t",
        public int              $linesBetweenProperties = 0,
        public int              $linesBetweenMethods = 2,
        public int              $linesBetweenUseTypes = 0,
        public string           $returnTypeColon = ': ',
        public bool             $bracesOnNextLine = true,
        public bool             $singleParameterOnOneLine = false,
        public bool             $omitEmptyNamespaces = true,
        protected ?PhpNamespace $namespace = null
    )
    {
        $this->dumper = $dumper ?? new Dumper();
    }

}
