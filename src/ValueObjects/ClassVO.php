<?php

namespace Birb\FancyStubsCodegen\ValueObjects;

use ReflectionClass;
use Nette\PhpGenerator\Attribute;

readonly class ClassVO
{
    public string $code;

    /**
     * @param ReflectionClass $reflectionClass
     * @param string $name
     * @param string|null $comment
     * @param array<Attribute> $additionalAttributes
     */
    public function __construct(
        private ReflectionClass $reflectionClass,
        public string           $name,
        public ?string          $comment = null,
        public array $additionalAttributes = []
    )
    {
        $this->code = file_get_contents($this->reflectionClass->getFileName());
    }
}
