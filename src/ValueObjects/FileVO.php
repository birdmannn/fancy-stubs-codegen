<?php

namespace Birb\FancyStubsCodegen\ValueObjects;

readonly class FileVO
{

    public function __construct(
        public string        $comment
    )
    {
    }

}
