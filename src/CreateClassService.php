<?php

namespace Birb\FancyStubsCodegen;

use Birb\FancyStubsCodegen\CreateClassService\Attributes\DefinerAttribute;
use Birb\FancyStubsCodegen\CreateClassService\Attributes\Exception\NotDefinedException;
use Birb\FancyStubsCodegen\CreateClassService\ClassLikeWrapper;
use Birb\FancyStubsCodegen\ValueObjects\PrinterVO;
use Birb\FancyStubsCodegen\ValueObjects\AttributeTokenVo;
use Birb\FancyStubsCodegen\Wrappers\PrinterWrapper;
use Nette\PhpGenerator\Constant;
use Nette\PhpGenerator\Printer;
use Nette\PhpGenerator\Property;
use ReflectionException;

readonly class CreateClassService
{
    private Printer $printer;

    public function __construct(
        private ClassLikeWrapper $classLikeWrapper,
        PrinterVO                $printerVO
    )
    {
        $this->printer = new PrinterWrapper(
            $printerVO
        );
    }

    /**
     * @return void
     * @throws NotDefinedException
     * @throws ReflectionException
     */
    public function applyDefinersCalculateToTokens(): void
    {
        foreach ($this->getAttributeTokens() as $attributeToken) {
            foreach ($attributeToken->getAttributes() as $attribute) {
                $attributeClass = $attribute->getName();
                if ($attributeClass === DefinerAttribute::class) {
                    /** @var DefinerAttribute $attributeClass */
                    $attributeClass::newInstanceOfDefiner(
                        $attribute,
                        $attributeToken,
                        $this->classLikeWrapper->getClassTypeContext()
                    )->handle();
                }
                $attributeToken->setAttributes($attributeToken->getAttributes());
            }
        }
    }

    /**
     * @throws ReflectionException
     * @throws NotDefinedException
     */
    public function build(): ?string
    {
        $this->classLikeWrapper->classType->setName($this->classLikeWrapper->classVO->name);
        if ($this->classLikeWrapper->classVO->comment) {
            $this->classLikeWrapper->classType->addComment($this->classLikeWrapper->classVO->comment);
        }
        $this->applyDefinersCalculateToTokens();
        return sprintf("<?php\n\n%s ", $this->printer->printNamespace($this->classLikeWrapper->namespace));
    }

    /**
     * Find definers in attributes
     *
     * @return array<AttributeTokenVo>
     */
    private function getAttributeTokens(): array
    {
        return collect(array_merge(
            $this->classLikeWrapper->classType->getProperties(),
            $this->classLikeWrapper->classType->getConstants(),
            [
                $this->classLikeWrapper
            ]
        ))->map(function (
            Property|Constant|ClassLikeWrapper $protoToken
        ) {
            return new AttributeTokenVo(
                $protoToken
            );
        })->toArray();
    }
}
