<?php

namespace Birb\FancyStubsCodegen\WithContextClassAbstract;

use Birb\FancyStubsCodegen\WithContextClassAbstract;

readonly abstract class HandlerContainer extends WithContextClassAbstract
{
    public function handle(): void {

    }
}
