<?php

namespace Birb\FancyStubsCodegen\WithContextClassAbstract;

use Birb\FancyStubsCodegen\WithContextClassAbstract;

readonly abstract class PutDefaultValue extends WithContextClassAbstract
{
    public abstract function getValue(): string;

    public function handle(): void
    {
        $this->attributeTokenVo->setValue($this->getValue());
    }
}
