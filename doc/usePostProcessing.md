After generation code you may be needed to use on this file some postprocessing code   

Base postprocessing look like that: 

```php

// Application.php
namespace App;

use \Birb\FancyStubsCodegen\Postprocessing\PhpCbfOnStringCode;

class Application {
    
    public function __construct() {
        $codeForPostprocessing = '<?php class Gigachad extends Model {public const TABLE = \'gigachads\';protected $table = self::TABLE;protected $primaryKey = \'gigachad_id\';}';
        
        $codeForPostprocessing = (new PhpCbfOnStringCode('PSR2.xml'))->build($codeForPostprocessing);
        
        file_put_contents('file-psr2.php', $codeForPostprocessing);
    }    
}
```

```php
// file-psr2.php

<?php class Gigachad extends Model {public const TABLE = 'gigachads';
    protected $table = self::TABLE;
    protected $primaryKey = 'gigachad_id';
}

```


