First thing you need implement any class. Main rules to this class:

1. May use any extends, implements, dependency, function definition
2. May include attribute DefinerAttribute with definerClass on class, field, const or anything attribute (in stub template) this will cause call his \`handle\` method with class context which handle this event. (see in next page example for generation class with context first created stub) In this definer you may generate other file or anything you want.
3. And may use PhpCbfOnStringCode for fixe code style (used [phpcbf](https://github.com/PHPCSStandards/PHP_CodeSniffer/) for generate) for your raw code generated class 

See in examples:

```php
<?php
namespace App\Stabs;

use Illuminate\Database\Eloquent\Model;

// GigachadStab.php
class GigachadStab extends Model {
    public const TABLE = '';
    protected $table   =  self::TABLE;
    
    #[DefinerAttribute(
        definerClass: LaravelConventForIdsUseClassNameContextPutDefaultValue::class
    )]
    protected $primaryKey;
}

// MakeNewModelClass.php

use Birb\FancyStubsCodegen\CreateClassService;
use Birb\FancyStubsCodegen\CreateClassService\Attributes\DefinerAttribute;
use Birb\FancyStubsCodegen\CreateClassService\Definers\LaravelConventForTableUseClassNameContextPutDefaultValue;
use Birb\FancyStubsCodegen\CreateClassService\Definers\LaravelConventForIdsUseClassNameContextPutDefaultValue;
use Birb\FancyStubsCodegen\CreateClassService\Definers\SprintfCamelCaseSingularId;
use Birb\FancyStubsCodegen\ValueObjects\ClassVO;
use Birb\FancyStubsCodegen\ValueObjects\PrinterVO;
use Birb\FancyStubsCodegen\ValueObjects\FileVO;
use App\Stabs\GigachadStab;

final class MakeNewModelClass {
    readonly private CreateClassService $classService;

    public function __construct(
        readonly string $className
    ) {
        $this->classService = new CreateClassService(
            new ClassLikeWrapper(
                classVO: new ClassVO(
                    new ReflectionClass(
                        GigachadStab::class
                    ),
                    $className,
                    'Test generated stub'
                ),
                namespace: (new PhpNamespace("App\Test")),
                printerVO: new PrinterVO(
                    resolveTypes: true,
                    omitEmptyNamespaces: false
                )
            )
        );
    }
    
    public handle(): void
    {
        file_put_contents('file.php', $this->classService->build());
    }
}

// Application.php

class Application {
    
    public function __construct() {
        (new MakeNewModelClass(
            'Gigachad'
        ))->handle();
    }
    
}
```

Result:

```php
<?php

namespace App\Test;

use Illuminate\Database\Eloquent\Model;

// file.php

class Gigachad extends Model {
    public const TABLE = 'gigachads';
    protected $table      =  self::TABLE;
    
    protected $primaryKey =  'gigachad_id';
}
```

p.s. You may use this library for code examples in your documentation, updated after update you project for code generation (I use this like that)
